import sys

import numpy as np
from keras.layers import LSTM, Dense
from keras.models import Sequential
from keras.models import load_model


class PillLSTMClassifier(object):
    def __init__(self):
        self.model = Sequential()
        self.time_steps = 28
        self.n_units = 128
        self.n_inputs = 244
        self.n_classes = 2
        self.batch_size = 128
        self.n_epochs = 5
        self.data_loaded = False
        self.trained = False

    def create_model(self):
        self.model.add(LSTM(self.n_units, input_shape=(self.time_steps, self.n_inputs)))
        self.model.add(Dense(self.n_classes, activation='softmax'))

        self.model.compile(loss='categorical_crossentropy',
                           optimizer='rmsprop',
                           metrics=['accuracy'])

    def load_data(self):
        self.train_images = np.load('./Pill_data/train_Pills.npz', 'r')
        self.train_labels = np.load('./Pill_data/train_Pills_labels.npz', 'r')
        self.test_images = np.load('./Pill_data/test_Pills.npz', 'r')
        self.test_labels = np.load('./Pill_data/test_Pills_labels.npz', 'r')
        self.data_loaded = True

    def train(self, save_model=False):
        self.create_model()
        if not self.data_loaded:
            self.load_data()

        x_train = [x.reshape((-1, self.time_steps, self.n_inputs)) for x in self.train_images]
        x_train = np.array(x_train).reshape((-1, self.time_steps, self.n_inputs))

        self.model.fit(x_train, self.train_labels,
                       batch_size=self.batch_size, epochs=self.n_epochs, shuffle=False)

        self.trained = True

        if save_model:
            self.model.save("./saved_model/Pill-lstm-model.h5")

    def evaluate(self, model=None):
        if self.trained is False and model is None:
            errmsg = "Not Trained, Model Not Found!"
            print(errmsg, file=sys.stderr)
            sys.exit(0)

        if not self.data_loaded:
            self.load_data()

        x_test = [x.reshape((-1, self.time_steps, self.n_inputs)) for x in self.test_images]
        x_test = np.array(x_test).reshape((-1, self.time_steps, self.n_inputs))

        model = load_model(model) if model else self.model
        test_loss = model.evaluate(x_test, self.test_labels)
        print(test_loss)
