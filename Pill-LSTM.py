from PillLSTMCLassifier import PillLSTMClassifier


if __name__ == "__main__":
    lstm_classifier = PillLSTMClassifier()
    lstm_classifier.train(save_model=True)
    lstm_classifier.evaluate()
    # lstm_classifier.evaluate(model="./saved_model/Pill-lstm-model.h5")
